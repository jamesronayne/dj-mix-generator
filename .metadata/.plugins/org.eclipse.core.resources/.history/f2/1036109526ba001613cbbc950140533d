package testing;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import cteq.CteQEventPoint;
import cteq.CteQFingerprint;
import cteq.CteQStrategy;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import util.NextTrack;

/**
 * These tests require the database to be available 
 * @author James Ronayne
 */
public class CteQStrategyTest {
	
	ObservableList<NextTrack> selectedTracklist = FXCollections.observableArrayList();;
	ObservableList<String> precedingTracklist = FXCollections.observableArrayList();;
	ObservableList<String> trailingTracklist = FXCollections.observableArrayList();;
	
	String testDir = "H:/test";
	String testOutputPlaylist = testDir + "/testOutputPlaylist.txt";
	String testOutput = testDir + "/testOutput.wav";
	
	CteQStrategy strat = new CteQStrategy();
	
	NextTrack nt1 = new NextTrack("testtrack1", "105503", 0, "http://www.1001tracklists.com/tracklist/47943_digitalism-crumb-mixtape-10-2013-12-10.html");
	NextTrack nt2 = new NextTrack("testtrack2", "44051", 0, "http://www.1001tracklists.com/tracklist/14251_felix-cartal-weekend-workout-14-2012-06-01.html");
	String mixPath1 = "H:/mixDownloads/105503/audio";
	String mixPath2 = "H:/mixDownloads/44051/audio";
	
	// Initialises CteQStrategy adding dummy playlist data
	private void initialise() {
		selectedTracklist.add(nt1);
		selectedTracklist.add(nt2);
		precedingTracklist.add("testPreceding");
		trailingTracklist.add("testTrailing");
		
		ArrayList<String> mixPaths = new ArrayList<String>();
		mixPaths.add(mixPath1);
		mixPaths.add(mixPath2);
		
		strat.setDataAndPrintPlaylist(selectedTracklist, precedingTracklist, trailingTracklist, testOutput, testOutputPlaylist, testDir, "H:/test/temp", mixPaths, false);
	}

	@Test
	public void outputPlaylistTest() {
		initialise();
		
		strat.outputPlaylist();
		
		Path path = FileSystems.getDefault().getPath(testOutputPlaylist);
		List<String> lines = null;
		
		
		try {
			lines = Files.readAllLines(path);
			// Checks play list file contains dummy play list data
			for(String str : lines) {
				Assert.assertEquals(true, (str.contains("testPreceding") || str.contains("testtrack1")
						|| str.contains("testtrack2") || str.contains("testTrailing")) );
				System.out.println(str);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void fingerprintsToHashTest() {
		CteQEventPoint eventPoint = new CteQEventPoint(0,0,0f,0f);
		CteQFingerprint finger = new CteQFingerprint(eventPoint, eventPoint, eventPoint);
		ArrayList<CteQFingerprint> arr = new ArrayList<CteQFingerprint>();
		arr.add(finger);
		
		HashMap<Integer, CteQFingerprint> result = strat.fingerprintsToHash(arr);
		
		// Checks resulting hashmap contains correct hash and checks size is one
		Assert.assertTrue(result.containsKey(finger.hash()));
		Assert.assertTrue(arr.size() == 1);
	}
	
	@Test
	public void synchronizeTest() {
		double[] offsets = strat.synchronize(3, mixPath1, mixPath2);
		double[] correctOffsets = {2132.95, 2399.71};
		
		// Checks offsets found match the correct offsets it should have found
		Assert.assertTrue(Math.abs(offsets[0] - correctOffsets[0]) < 0.1
				&& Math.abs(offsets[1] - correctOffsets[1]) < 0.1);
	}
	
	@Test
	public void generateTest() {
		initialise();
		
		strat.generate();
		
		// Simple check that files were generated
		Assert.assertTrue(new File(testOutput).exists());
		Assert.assertTrue(new File(testOutputPlaylist).exists());
	}
}
