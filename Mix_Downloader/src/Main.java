
/***************************************************************************
*                                                     								*
* DJ Mix Generator                                       					 		*
* Copyright (C) 2016 - James Ronayne                      							*
*                                                                          			*
* This program is free software: you can redistribute it and/or modify     			*
* it under the terms of the GNU Affero General Public License as           			*
* published by the Free Software Foundation, either version 3 of the       			*
* License, or (at your option) any later version.                          			*
*                                                                          			*
* This program is distributed in the hope that it will be useful,          			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of           			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            			*
* GNU Affero General Public License for more details.                      			*
*                                                                          			*
* You should have received a copy of the GNU Affero General Public License 			*
* along with this program.  If not, see <http://www.gnu.org/licenses/>     			*
*                                                                          			*
*************************************************************************************
*																					*
*																					*
*    8888888b. 888888      888b     d888 d8b                                      	*
*    888  "Y88b  "88b      8888b   d8888 Y8P                                      	*
*    888    888   888      88888b.d88888                                          	*
*    888    888   888      888Y88888P888 888 888  888                             	*
*    888    888   888      888 Y888P 888 888 `Y8bd8P'                             	*
*    888    888   888      888  Y8P  888 888   X88K                               	*
*    888  .d88P   88P      888   "   888 888 .d8""8b.                             	*
*    8888888P"    888      888       888 888 888  888                             	*
*               .d88P                                                             	*
*             .d88P"                                                              	*
*            888P"                                                                	*
*     .d8888b.                                             888                    	*
*    d88P  Y88b                                            888                    	*
*    888    888                                            888                    	*
*    888         .d88b.  88888b.   .d88b.  888d888 8888b.  888888 .d88b.  888d888 	*
*    888  88888 d8P  Y8b 888 "88b d8P  Y8b 888P"      "88b 888   d88""88b 888P"   	*
*    888    888 88888888 888  888 88888888 888    .d888888 888   888  888 888     	*
*    Y88b  d88P Y8b.     888  888 Y8b.     888    888  888 Y88b. Y88..88P 888     	*
*     "Y8888P88  "Y8888  888  888  "Y8888  888    "Y888888  "Y888 "Y88P"  888     	*
*                                                                                 	*
************************************************************************************/

import jodd.http.HttpBrowser;
import jodd.http.HttpException;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;

import static jodd.jerry.Jerry.jerry;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author James Ronayne
 */
public class Main {
	private static Queue<String> djMixLinks = new LinkedList<String>();
	private static ArrayList<String> addedMixes = new ArrayList<String>();
	private static ArrayList<String> parsedTracks = new ArrayList<String>();
	private static int count = 1;

	private static long prevTime = 0;
	// Establish the connection to the database
	private static String url = "jdbc:mysql://127.0.0.1:3306/mix";
	private static Connection conn;

	static HttpBrowser http = new HttpBrowser();

	// Tells program where to save downloaded mixes
	private static final String BASE_DIRECTORY_MIXES = "H:/mixDownloads/";
	// Time delay between requests to 1001tracklists to avoid being blocked
	private static final long TIME_DELAY = 5000; // ms

	// first command line argument should be the seed url which should be a mix
	// page from 1001tracklists
	public static void main(String[] args) {
		String baseUrl = args[0];

		try {
			// Load the JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection(url, "root", "djgenerator");

			djMixLinks.offer(baseUrl);

			// count is just the id number used to save audio file in specific
			// folder
			// sql statement just grabs the folder number used for last mix
			// added and adds one
			Statement select = conn.createStatement();
			String sql = "SELECT Filepath FROM mixes ORDER BY id DESC limit 1";
			ResultSet rs = select.executeQuery(sql);
			rs.next();

			String countString = rs.getString("Filepath");
			countString = countString.substring(BASE_DIRECTORY_MIXES.length(), countString.lastIndexOf('/'));
			count = (Integer.parseInt(countString) + 1);

			workThroughQueue();
		} catch (SQLException e) {
			System.out.println("Failed to connect to mysql server");
			e.getMessage();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void workThroughQueue() {

		while (djMixLinks.peek() != null) {
			Date time = new Date();

			// time delay to prevent being banned
			if (time.getTime() < prevTime + TIME_DELAY) {
				// if TIME_DELAY hasn't past since last request then spin until
				// has
				while (time.getTime() < prevTime + TIME_DELAY) {
					time = new Date();
				}
			}

			String mixUrl = djMixLinks.poll();
			mixUrl = "http://www.1001tracklists.com" + mixUrl;
			int index = mixUrl.lastIndexOf("#");
			if (index != -1) {
				mixUrl = mixUrl.substring(0, index);
			}
			System.out.println(mixUrl);

			if (addedMixes.contains(mixUrl)) {
				continue;
			}

			// Gets mix page html
			HttpResponse response;
			try {
				response = HttpRequest.get(mixUrl).send();
			} catch (HttpException e) {
				System.out.println("Error: HTTPException skipping");
				continue;
			}

			String body = response.body();

			// Record time since request
			prevTime = time.getTime();

			// Save html from mix page in temp file
			writeHtmlTemp(body);

			if (body != null) {
				ArrayList<String> downloadLinks = new ArrayList<String>();

				// Allows JQuery operations to be performed on the mix page
				Jerry doc = jerry(body);
				// Extracts playlist info and saves in a Playlist object
				Playlist playlist = getPlaylist(doc);

				if (playlist != null) {

					String mixName = playlist.getMixName();

					// Simple check for radio shows
					// the case-insensitive pattern we want to search for
					Pattern p = Pattern.compile("radio|Essential mix|Podcast|@|festival|at|events",
							Pattern.CASE_INSENSITIVE);
					Matcher m = p.matcher(mixName);

					// see if we found a match eg. its a radio show
					if (m.find()) {
						System.out.println("Mix is a radio show so skipping:");
						System.out.println(mixUrl);

					} else {

						downloadLinks = getDownloadLinks(doc);

						if (downloadLinks != null) {

							System.out.println(downloadLinks.toString());

							// query sql database if not there insert
							try {

								String dir = count + "/";
								String mixFilepath = BASE_DIRECTORY_MIXES + dir + "audio.mp3";

								System.out.println(mixFilepath);

								// Check to see if mix already exists in
								// database based upon url
								Statement select = conn.createStatement();
								String sql = "SELECT * FROM mixes WHERE Name = \"" + playlist.getMixName() + "\"";
								ResultSet rs = select.executeQuery(sql);

								// If resultSet is empty then add mix
								if (!rs.next()) {

									Statement insert = conn.createStatement();
									sql = "INSERT INTO playlists (id) VALUES (" + count + ")";
									insert.executeUpdate(sql);

									sql = "INSERT INTO mixes SET Name = \"" + playlist.getMixName() + "\", URL = \""
											+ mixUrl + "\", playlist_id = (SELECT id FROM playlists WHERE id = " + count
											+ "), Filepath = \"" + mixFilepath + "\"";
									insert.executeUpdate(sql);

									for (int i = 0; i < playlist.getArtists().size(); i++) {
										try {
											sql = "INSERT INTO tracks (artist, name) VALUES (\""
													+ playlist.getArtists().get(i) + "\", \""
													+ playlist.getTracks().get(i) + "\")";
											insert.executeUpdate(sql);
										} catch (SQLException e) {
											// Do nothing as song is already in
											// tracks table
										}

										sql = "INSERT INTO playlists_songs (playlist_id, track_id, track_number) VALUES ((SELECT id FROM playlists WHERE id = "
												+ count + "), (SELECT track_id FROM tracks WHERE name = \""
												+ playlist.getTracks().get(i) + "\"), " + i + ")";
										insert.executeUpdate(sql);
									}

									insert.close();

									String path = downloadMixesWithAria2(downloadLinks);

									// If couldn't download mix undo entry in
									// database
									if (path == null) {
										System.out.println(
												"Error: mix could not be downloaded removing from database then skipping");
										System.out.println(mixUrl);

										Statement delete = conn.createStatement();
										sql = "DELETE FROM mixes WHERE url = \"" + mixUrl + "\"";
										delete.executeUpdate(sql);

										sql = "DELETE FROM playlists_songs WHERE playlist_id = " + count;
										delete.executeUpdate(sql);

										sql = "DELETE FROM playlists WHERE id = " + count;
										delete.executeUpdate(sql);

										for (int i = 0; i < playlist.getArtists().size(); i++) {

											try {
												sql = "DELETE FROM playlists WHERE artist = \""
														+ playlist.getArtists().get(i) + "\" AND name = \""
														+ playlist.getTracks().get(i) + "\"";
												delete.executeUpdate(sql);
											} catch (SQLException e) {
												// Do nothing as song is already
												// in tracks table
											}

										}

										delete.close();
									}

									addedMixes.add(mixUrl);
								} else {
									System.out.println("Error: mix already in database skip");
								}

								select.close();

							} catch (Exception e) {
								System.err.println("D'oh! Got an exception!");
								System.err.println(e.getMessage());
							}
						} else {
							System.out.println("Error: No download links skipping");
						}
					}

					// If less than 200 dj mix links in djMixLinks then add more
					if (djMixLinks.size() < 200) {

						for (int i = 0; i < playlist.getLinks().size(); i++) {
							if (playlist.getLinks().get(i) != null) {
								ArrayList<String> mixLinks = parseTrackPageForMixLinks(playlist.getLinks().get(i));
								parsedTracks.add(playlist.getLinks().get(i));
								if (mixLinks != null) {
									for (int j = 0; j < mixLinks.size(); j++) {
										if ((!(djMixLinks.contains(mixLinks.get(j)))) && mixLinks.get(j) != null) {
											djMixLinks.offer(mixLinks.get(j));
										}
									}
								}
							}
						}
					}

				} else {
					System.out.println("Error: playlist is null, trying next mix");
				}
			}

			count++;
		}
	}

	/**
	 * Saves html in temp.html
	 * 
	 * @param html
	 *            String containing html of a webpage
	 * @return
	 */
	public static boolean writeHtmlTemp(String html) {
		// Writes html page to temp.html file for the microdata.py script to
		// process
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("temp.html"), "utf-8"));
			writer.write(html);
		} catch (IOException e) {
			System.out.println("failed to write temp.html exiting...");
			return false;
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
				/* ignore */}
		}
		return true;
	}

	/**
	 * Pass Jerry object of page and returns an arraylist of download links
	 * 
	 * @param body
	 * @return
	 */
	public static ArrayList<String> getDownloadLinks(Jerry body) {
		ArrayList<String> downloadLinks = new ArrayList<String>();

		int numLinks = body.$("body div div form div div div table tr td a").filter(downloadLinkCheck).length();

		for (int i = 0; i < numLinks; i++) {
			downloadLinks.add((body.$("body div div form div div div table tr td a").filter(downloadLinkCheck).eq(i)
					.attr("href")));
		}

		if (downloadLinks.size() == 0) {
			return null;
		} else {
			return downloadLinks;
		}
	}

	/**
	 * JerryFunction to filter for download links
	 */
	private static JerryFunction downloadLinkCheck = new JerryFunction() {
		public boolean onNode(Jerry $this, int index) {
			try {
				if ($this.attr("class").equals("floatL iconHeight32"))
					return true;
				else
					return false;
			} catch (NullPointerException e) {
				return false;
			}
		}
	};

	/**
	 * Gets playlist from web page return null if not a complete playlist so
	 * shouldn't add mix if returns null
	 * 
	 * @param body
	 * @return
	 */
	public static Playlist getPlaylist(Jerry body) {

		// Add check if Id'd on page is all tracks
		try {
			if (!(body.$("div div form div div div table tr td span").get()[2].getAttribute("class")
					.equals("greenTxt"))) {
				System.out.println("Error: Not all tracks were IDed");
				return null;
			}
		} catch (NullPointerException e) {
			System.out.println("Error: IDed check null pointer exception");
			return null;
		} catch (ArrayIndexOutOfBoundsException e) {
			if (body.$("div div form div div div table tr td span").length() == 1) {
				System.out.println("Error: Not all tracks were IDed");
				return null;
			}
			;
			System.out.println("Error: IDed check ArrayIndexOutOfBoundsException");

			return null;
		}

		Playlist playlist = null;
		;
		String s = "";
		String output = "";

		try {
			// uses microdata.py to parse microdata from html and output a json
			// object which contains the playlist
			Process p = Runtime.getRuntime().exec("python microdata.py temp.html");

			BufferedReader stdOutput = new BufferedReader(new InputStreamReader(p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

			// read the output from the command
			while ((s = stdOutput.readLine()) != null) {
				output = output + s;
			}

			JSONObject json = new JSONObject(output);

			try {
				// new playlist() parses the json and extracts the playlist into
				// an arraylist
				playlist = new Playlist(json);
			} catch (JSONException e) {
				// Abandon this mix if no complete tracklist
				return null;
			}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of microdata.py (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}

		} catch (IOException e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);
		}

		return playlist;
	}

	/**
	 * Find links to mixes which feature track from the track page given
	 * 
	 * @param url
	 * @return
	 */
	public static ArrayList<String> parseTrackPageForMixLinks(String url) {
		if (parsedTracks.contains(url))
			return null;

		Date time = new Date();
		if (time.getTime() < prevTime + TIME_DELAY) {
			// if 30 seconds haven't past since last request then spin until has
			while (time.getTime() < prevTime + TIME_DELAY) {
				time = new Date();
			}
		}
		System.out.println("Parsing: " + url);

		ArrayList<String> mixLinks = new ArrayList<String>();
		HttpResponse response = HttpRequest.get(url).send();
		String body = response.body();

		// Record time since request
		prevTime = time.getTime();

		Jerry doc = jerry(body);

		try {

			for (int i = 0; i < doc.$("table tr td div span.tlLink a").attr("href").length(); i++) {
				mixLinks.add(doc.$("table tr td div span.tlLink a").eq(i).attr("href"));
			}

			return mixLinks;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Can be given an array list of download links for the same file, if one
	 * fails the next link is tried
	 * 
	 * @param downloadLinks
	 *            array list of download links for same file
	 * @return Path to the downloaded file
	 */
	public static String downloadMixesWithAria2(ArrayList<String> downloadLinks) {
		String s = "";

		// count is just the id number for the mix in the MySQL database
		String dir = count + "/";

		// Might have to change to backwards slash for windows
		System.out.println(dir + "audio");

		try {
			for (int i = 0; i < downloadLinks.size(); i++) {
				String output = "";
				String error = "";

				Process p = Runtime.getRuntime().exec("python youtube_dl/__main__.py -g " + downloadLinks.get(i));

				BufferedReader stdOutput = new BufferedReader(new InputStreamReader(p.getInputStream()));

				BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

				// read the output from the command
				System.out.println("Here is the standard output of youtube-dl:");
				while ((s = stdOutput.readLine()) != null) {
					System.out.println(s);
					output = output + s;
				}

				// read any errors from the attempted command
				System.out.println("Here is the standard error of youtube-dl (if any):");
				while ((s = stdError.readLine()) != null) {
					System.out.println(s);
					error = error + s;
				}

				if (!error.contains("ERROR")) {

					// If between 6 and 2 at night then limit download else
					// don't
					Process p2;
					int hourOfDay = Calendar.HOUR_OF_DAY;
					if (hourOfDay > 15 || hourOfDay < 2) {
						p2 = Runtime.getRuntime()
								.exec("H:/aria2/aria2c.exe --max-overall-download-limit=4M -x 16 -j 2000 -s 2000 -k 1M -d "
										+ BASE_DIRECTORY_MIXES + dir + " -o audio " + output);
					} else {
						p2 = Runtime.getRuntime().exec("H:/aria2/aria2c.exe -x 16 -j 2000 -s 2000 -k 1M -d "
								+ BASE_DIRECTORY_MIXES + dir + " -o audio " + output);
					}

					BufferedReader stdOutputAria2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));

					BufferedReader stdErrorAria2 = new BufferedReader(new InputStreamReader(p2.getErrorStream()));

					// read the output from the command
					System.out.println("Here is the standard output of aria2:");
					while ((s = stdOutputAria2.readLine()) != null) {
						System.out.println(s);
					}

					// read any errors from the attempted command
					System.out.println("Here is the standard error of aria2 (if any):");
					while ((s = stdErrorAria2.readLine()) != null) {
						System.out.println(s);
					}

				}

				// If file exists download was successful else try next link
				File file = new File(BASE_DIRECTORY_MIXES + dir + "audio");
				if (file.exists()) {
					break;
				} else {
					continue;
				}
			}

			// If file exists download was successful else return null and
			// handled in workThroughQueue()
			File file = new File(BASE_DIRECTORY_MIXES + dir + "audio");
			if (file.exists()) {
				return BASE_DIRECTORY_MIXES + dir + "audio";
			} else {
				return null;
			}
		} catch (IOException e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);
		}

		return null;
	}
}