
/***************************************************************************
*                                                     								*
* DJ Mix Generator                                       					 		*
* Copyright (C) 2016 - James Ronayne                      							*
*                                                                          			*
* This program is free software: you can redistribute it and/or modify     			*
* it under the terms of the GNU Affero General Public License as           			*
* published by the Free Software Foundation, either version 3 of the       			*
* License, or (at your option) any later version.                          			*
*                                                                          			*
* This program is distributed in the hope that it will be useful,          			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of           			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            			*
* GNU Affero General Public License for more details.                      			*
*                                                                          			*
* You should have received a copy of the GNU Affero General Public License 			*
* along with this program.  If not, see <http://www.gnu.org/licenses/>     			*
*                                                                          			*
*************************************************************************************
*																					*
*																					*
*    8888888b. 888888      888b     d888 d8b                                      	*
*    888  "Y88b  "88b      8888b   d8888 Y8P                                      	*
*    888    888   888      88888b.d88888                                          	*
*    888    888   888      888Y88888P888 888 888  888                             	*
*    888    888   888      888 Y888P 888 888 `Y8bd8P'                             	*
*    888    888   888      888  Y8P  888 888   X88K                               	*
*    888  .d88P   88P      888   "   888 888 .d8""8b.                             	*
*    8888888P"    888      888       888 888 888  888                             	*
*               .d88P                                                             	*
*             .d88P"                                                              	*
*            888P"                                                                	*
*     .d8888b.                                             888                    	*
*    d88P  Y88b                                            888                    	*
*    888    888                                            888                    	*
*    888         .d88b.  88888b.   .d88b.  888d888 8888b.  888888 .d88b.  888d888 	*
*    888  88888 d8P  Y8b 888 "88b d8P  Y8b 888P"      "88b 888   d88""88b 888P"   	*
*    888    888 88888888 888  888 88888888 888    .d888888 888   888  888 888     	*
*    Y88b  d88P Y8b.     888  888 Y8b.     888    888  888 Y88b. Y88..88P 888     	*
*     "Y8888P88  "Y8888  888  888  "Y8888  888    "Y888888  "Y888 "Y88P"  888     	*
*                                                                                 	*
************************************************************************************/

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author James Ronayne
 */
public class Playlist {
	private ArrayList<String> artists = new ArrayList<String>();
	private ArrayList<String> tracks = new ArrayList<String>();
	private ArrayList<String> links = new ArrayList<String>();
	private String mixName = null;

	/**
	 * Takes json object generated using microdata.py on a 1001Tracklists mix
	 * page, and extracts playlist info
	 * 
	 * @param json
	 * @throws JSONException
	 */
	public Playlist(JSONObject json) throws JSONException {
		mixName = ((String) ((JSONObject) json.getJSONArray("items").get(0)).getJSONObject("properties")
				.getJSONArray("name").get(0));

		JSONArray playlistJson = json.getJSONArray("items");
		playlistJson = ((JSONObject) playlistJson.get(1)).getJSONObject("properties").getJSONArray("tracks");

		for (int i = 0; i < playlistJson.length(); i++) {
			try {
				links.add("http://www.1001tracklists.com"
						+ ((JSONObject) playlistJson.get(i)).getJSONObject("properties").getJSONArray("url").get(0));
			} catch (JSONException e) {
				links.add(null);
			}
			artists.add((String) ((JSONObject) playlistJson.get(i)).getJSONObject("properties").getJSONArray("byArtist")
					.get(0));
			tracks.add((String) ((JSONObject) playlistJson.get(i)).getJSONObject("properties").getJSONArray("name")
					.get(0));
		}

	}

	/**
	 * Returns mix name
	 * 
	 * @return
	 */
	public String getMixName() {
		return mixName;
	}

	/**
	 * Returns array list of artists
	 * 
	 * @return
	 */
	public ArrayList<String> getArtists() {
		return artists;
	}

	/**
	 * Returns array list of tracks
	 * 
	 * @return
	 */
	public ArrayList<String> getTracks() {
		return tracks;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<String> getLinks() {
		return links;
	}
}
