/***************************************************************************
*                                                     								*
* DJ Mix Generator                                       					 		*
* Copyright (C) 2016 - James Ronayne                      							*
*                                                                          			*
* This program is free software: you can redistribute it and/or modify     			*
* it under the terms of the GNU Affero General Public License as           			*
* published by the Free Software Foundation, either version 3 of the       			*
* License, or (at your option) any later version.                          			*
*                                                                          			*
* This program is distributed in the hope that it will be useful,          			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of           			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            			*
* GNU Affero General Public License for more details.                      			*
*                                                                          			*
* You should have received a copy of the GNU Affero General Public License 			*
* along with this program.  If not, see <http://www.gnu.org/licenses/>     			*
*                                                                          			*
*************************************************************************************
*																					*
*																					*
*    8888888b. 888888      888b     d888 d8b                                      	*
*    888  "Y88b  "88b      8888b   d8888 Y8P                                      	*
*    888    888   888      88888b.d88888                                          	*
*    888    888   888      888Y88888P888 888 888  888                             	*
*    888    888   888      888 Y888P 888 888 `Y8bd8P'                             	*
*    888    888   888      888  Y8P  888 888   X88K                               	*
*    888  .d88P   88P      888   "   888 888 .d8""8b.                             	*
*    8888888P"    888      888       888 888 888  888                             	*
*               .d88P                                                             	*
*             .d88P"                                                              	*
*            888P"                                                                	*
*     .d8888b.                                             888                    	*
*    d88P  Y88b                                            888                    	*
*    888    888                                            888                    	*
*    888         .d88b.  88888b.   .d88b.  888d888 8888b.  888888 .d88b.  888d888 	*
*    888  88888 d8P  Y8b 888 "88b d8P  Y8b 888P"      "88b 888   d88""88b 888P"   	*
*    888    888 88888888 888  888 88888888 888    .d888888 888   888  888 888     	*
*    Y88b  d88P Y8b.     888  888 Y8b.     888    888  888 Y88b. Y88..88P 888     	*
*     "Y8888P88  "Y8888  888  888  "Y8888  888    "Y888888  "Y888 "Y88P"  888     	*
*                                                                                 	*
************************************************************************************/

package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;

/**
 * Places pcm audio data into tamp.pcm or another specified file from the
 * decoded stream do this for multiple files and they will be concatenated into
 * a single file
 * 
 * @author James Ronayne
 *
 */
public class AudioStitcher implements AudioProcessor {

	int i = 0;
	FileOutputStream fos;
	byte[] audioBuffer;
	RandomAccessFile file;

	AudioStitcher() {
		try {
			this.fos = new FileOutputStream("E:\\backup\\temp.pcm", true);
			this.file = new RandomAccessFile("E:\\backup\\temp.pcm", "rw");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public AudioStitcher(File file) {
		try {
			this.fos = new FileOutputStream(file.getAbsolutePath(), true);
			this.file = new RandomAccessFile(file.getAbsolutePath(), "rw");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean process(AudioEvent audioEvent) {
		audioBuffer = audioEvent.getByteBuffer().clone();

		try {
			fos.write(audioBuffer);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return true;
	}

	@Override
	public void processingFinished() {
		// Finding trailing zeros that seem to be added for no reason and reduce
		// file size by the number of zeros
		int count = 0;
		if (audioBuffer != null) {
			for (int i = 0; i < audioBuffer.length; i++) {
				if (audioBuffer[i] == 0)
					count++;
				else if (audioBuffer[i] != 0)
					count = 0;
			}
		}

		try {
			fos.close();
			file.setLength(file.length() - count);
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
