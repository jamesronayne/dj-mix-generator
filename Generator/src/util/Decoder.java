/***************************************************************************
*                                                     								*
* DJ Mix Generator                                       					 		*
* Copyright (C) 2016 - James Ronayne                      							*
*                                                                          			*
* This program is free software: you can redistribute it and/or modify     			*
* it under the terms of the GNU Affero General Public License as           			*
* published by the Free Software Foundation, either version 3 of the       			*
* License, or (at your option) any later version.                          			*
*                                                                          			*
* This program is distributed in the hope that it will be useful,          			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of           			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            			*
* GNU Affero General Public License for more details.                      			*
*                                                                          			*
* You should have received a copy of the GNU Affero General Public License 			*
* along with this program.  If not, see <http://www.gnu.org/licenses/>     			*
*                                                                          			*
*************************************************************************************
*																					*
*																					*
*    8888888b. 888888      888b     d888 d8b                                      	*
*    888  "Y88b  "88b      8888b   d8888 Y8P                                      	*
*    888    888   888      88888b.d88888                                          	*
*    888    888   888      888Y88888P888 888 888  888                             	*
*    888    888   888      888 Y888P 888 888 `Y8bd8P'                             	*
*    888    888   888      888  Y8P  888 888   X88K                               	*
*    888  .d88P   88P      888   "   888 888 .d8""8b.                             	*
*    8888888P"    888      888       888 888 888  888                             	*
*               .d88P                                                             	*
*             .d88P"                                                              	*
*            888P"                                                                	*
*     .d8888b.                                             888                    	*
*    d88P  Y88b                                            888                    	*
*    888    888                                            888                    	*
*    888         .d88b.  88888b.   .d88b.  888d888 8888b.  888888 .d88b.  888d888 	*
*    888  88888 d8P  Y8b 888 "88b d8P  Y8b 888P"      "88b 888   d88""88b 888P"   	*
*    888    888 88888888 888  888 88888888 888    .d888888 888   888  888 888     	*
*    Y88b  d88P Y8b.     888  888 Y8b.     888    888  888 Y88b. Y88..88P 888     	*
*     "Y8888P88  "Y8888  888  888  "Y8888  888    "Y888888  "Y888 "Y88P"  888     	*
*                                                                                 	*
************************************************************************************/

package util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.io.UniversalAudioInputStream;

/**
 * 
 * @author James Ronayne
 *
 */
public class Decoder {

	/**
	 * Returns AudioDispatcher object of the complete audio file
	 * 
	 * @param resource
	 * @param targetSampleRate
	 * @param audioBufferSize
	 * @param bufferOverlap
	 * @return AudioDispatcher object to perform processing on
	 * @see be.tarsos.dsp.AudioDispatcher
	 */
	public static AudioDispatcher getDecodedStream(final String resource, final int targetSampleRate,
			int audioBufferSize, int bufferOverlap) {
		return decode(resource, targetSampleRate, audioBufferSize, bufferOverlap,
				" -i \"%resource%\" -vn -ar %sample_rate% -ac %channels% -sample_fmt s16 -f s16le pipe:1");
	}

	/**
	 * Decodes stream from position for duration using ffmpeg and reading in the
	 * pcm stream
	 * 
	 * @param resource
	 * @param targetSampleRate
	 * @param audioBufferSize
	 * @param bufferOverlap
	 * @param fromSeconds
	 * @param durationSeconds
	 * @param gain
	 *            Adjustment to be made to volume
	 * @return AudioDispatcher object to perform processing on
	 * @see be.tarsos.dsp.AudioDispatcher
	 */
	public static AudioDispatcher getDecodedStreamFromTo(final String resource, final int targetSampleRate,
			int audioBufferSize, int bufferOverlap, float fromSeconds, float durationSeconds, float gain) {
		String arguments;

		if (gain != 0f && !(Math.abs(durationSeconds - 0.0001f) < 0.00000001)) {
			// change gain and duration
			arguments = " -t " + durationSeconds + " -ss " + fromSeconds + " -i \"%resource%\" -vn -af \"volume=" + gain
					+ "dB\" -ar %sample_rate% -ac %channels% -sample_fmt s16 -f s16le pipe:1";
		} else if (gain != 0f && Math.abs(durationSeconds - 0.0001f) < 0.00000001) {
			// change gain and don't change duration
			arguments = " -ss " + fromSeconds + " -i \"%resource%\" -vn -af \"volume=" + gain
					+ "dB\"  -ar %sample_rate% -ac %channels% -sample_fmt s16 -f s16le pipe:1";
		} else if (!(Math.abs(durationSeconds - 0.0001f) < 0.00000001)) {
			// don't change gain and change duration
			arguments = " -t " + durationSeconds + " -ss " + fromSeconds
					+ " -i \"%resource%\" -vn -ar %sample_rate% -ac %channels% -sample_fmt s16 -f s16le pipe:1";
		} else {
			// don't change gain and don't change duration
			arguments = " -ss " + fromSeconds
					+ " -i \"%resource%\" -vn -ar %sample_rate% -ac %channels% -sample_fmt s16 -f s16le pipe:1";
		}

		return decode(resource, targetSampleRate, audioBufferSize, bufferOverlap, arguments);
	}

	/**
	 * Performs decoding based upon the given argument from getDecodedStream or
	 * getDecodedStreamFromTo
	 * 
	 * @param resource
	 *            Absolute path for audio file
	 * @param targetSampleRate
	 * @param audioBufferSize
	 * @param bufferOverlap
	 * @param arguments
	 *            The arguments to use for FFmpeg
	 * @return AudioDispatcher object to perform processing on
	 * @see be.tarsos.dsp.AudioDispatcher
	 */
	private static AudioDispatcher decode(final String resource, final int targetSampleRate, int audioBufferSize,
			int bufferOverlap, String arguments) {
		String pipeEnvironment;
		String pipeArgument;
		final String pipeCommand;

		// Use pipeEnvironment and pipeArgument depending on the platform
		if (System.getProperty("os.name").indexOf("indows") > 0) {
			pipeEnvironment = "cmd.exe";
			pipeArgument = "/C";
		} else if (new File("/bin/bash").exists()) {
			pipeEnvironment = "/bin/bash";
			pipeArgument = "-c";
		} else {
			throw new Error(
					"Decoding via a pipe will not work: Coud not find a command line environment (cmd.exe or /bin/bash)");
		}

		final int pipeBuffer = 10000;

		try {
			pipeCommand = "ffmpeg" + arguments;
			String command = pipeCommand;
			command = command.replace("%resource%", resource);
			command = command.replace("%sample_rate%", String.valueOf(targetSampleRate));
			command = command.replace("%channels%", "1");

			ProcessBuilder pb;
			pb = new ProcessBuilder(pipeEnvironment, pipeArgument, command);

			final Process process = pb.start();

			final InputStream stdOut = new BufferedInputStream(process.getInputStream(), pipeBuffer);

			return new AudioDispatcher(
					new UniversalAudioInputStream(stdOut,
							new TarsosDSPAudioFormat(targetSampleRate, 16, 1, true, false)),
					audioBufferSize, bufferOverlap);

		} catch (IOException e) {
			System.err.println("IO exception while decoding audio via sub process." + e.getMessage());
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Gets the peak gain for a given audio file.
	 * 
	 * @param resource
	 *            The absolute path for file to analyse
	 * @return float representing gain of resource
	 */
	public static float getGain(String resource) {
		String pipeEnvironment;
		String pipeArgument;
		final String pipeCommand;

		// Use sensible defaults depending on the platform
		if (System.getProperty("os.name").indexOf("indows") > 0) {
			pipeEnvironment = "cmd.exe";
			pipeArgument = "/C";
		} else if (new File("/bin/bash").exists()) {
			pipeEnvironment = "/bin/bash";
			pipeArgument = "-c";
		} else if (new File("/system/bin/sh").exists()) {
			// probably we are on android here
			pipeEnvironment = "/system/bin/sh";
			pipeArgument = "-c";
		} else {
			throw new Error(
					"Decoding via a pipe will not work: Coud not find a command line environment (cmd.exe or /bin/bash)");
		}

		try {
			String arguments = " -i " + resource + " -af \"volumedetect\" -f null NUL";
			pipeCommand = "ffmpeg" + arguments;
			String command = pipeCommand;

			System.out.println(command);

			ProcessBuilder pb;
			pb = new ProcessBuilder(pipeEnvironment, pipeArgument, command);

			final Process process = pb.start();

			String errorStream = "";
			InputStreamReader isr = new InputStreamReader(process.getErrorStream());
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				errorStream += line;
			}

			int index = errorStream.indexOf("max_volume: ") + 11;

			char ch = errorStream.toCharArray()[index];
			while (ch != 'd') {
				index++;
				ch = errorStream.toCharArray()[index];
			}

			String db = errorStream.substring(errorStream.indexOf("max_volume: ") + 11, index);
			;
			float dbAdjustment = Float.parseFloat(db);

			return dbAdjustment;

		} catch (IOException e) {
			System.out.println("IO exception while determining gain" + e.getMessage());
			e.printStackTrace();
		}
		return 0f;
	}
}
