/***************************************************************************
*                                                     								*
* DJ Mix Generator                                       					 		*
* Copyright (C) 2016 - James Ronayne                      							*
*                                                                          			*
* This program is free software: you can redistribute it and/or modify     			*
* it under the terms of the GNU Affero General Public License as           			*
* published by the Free Software Foundation, either version 3 of the       			*
* License, or (at your option) any later version.                          			*
*                                                                          			*
* This program is distributed in the hope that it will be useful,          			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of           			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            			*
* GNU Affero General Public License for more details.                      			*
*                                                                          			*
* You should have received a copy of the GNU Affero General Public License 			*
* along with this program.  If not, see <http://www.gnu.org/licenses/>     			*
*                                                                          			*
*************************************************************************************
*																					*
*																					*
*    8888888b. 888888      888b     d888 d8b                                      	*
*    888  "Y88b  "88b      8888b   d8888 Y8P                                      	*
*    888    888   888      88888b.d88888                                          	*
*    888    888   888      888Y88888P888 888 888  888                             	*
*    888    888   888      888 Y888P 888 888 `Y8bd8P'                             	*
*    888    888   888      888  Y8P  888 888   X88K                               	*
*    888  .d88P   88P      888   "   888 888 .d8""8b.                             	*
*    8888888P"    888      888       888 888 888  888                             	*
*               .d88P                                                             	*
*             .d88P"                                                              	*
*            888P"                                                                	*
*     .d8888b.                                             888                    	*
*    d88P  Y88b                                            888                    	*
*    888    888                                            888                    	*
*    888         .d88b.  88888b.   .d88b.  888d888 8888b.  888888 .d88b.  888d888 	*
*    888  88888 d8P  Y8b 888 "88b d8P  Y8b 888P"      "88b 888   d88""88b 888P"   	*
*    888    888 88888888 888  888 88888888 888    .d888888 888   888  888 888     	*
*    Y88b  d88P Y8b.     888  888 Y8b.     888    888  888 Y88b. Y88..88P 888     	*
*     "Y8888P88  "Y8888  888  888  "Y8888  888    "Y888888  "Y888 "Y88P"  888     	*
*                                                                                 	*
************************************************************************************/

package cteq;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author James Ronayne
 *
 */
public class CteQSyncMatch {

	private final List<float[]> matches = new ArrayList<float[]>();

	/**
	 * 
	 */
	public CteQSyncMatch() {
	}

	/**
	 * Add a match to the list.
	 * 
	 * @param startInReference
	 *            In seconds.
	 * @param stopInReference
	 *            In seconds.
	 * @param startInMatchingStream
	 *            In seconds.
	 * @param stopInMatchinStream
	 *            In seconds.
	 * @param score
	 *            The number of matching fingerprints.
	 */
	public void addMatch(float startInReference, float stopInReference, float startInMatchingStream,
			float stopInMatchinStream, float score) {
		float[] match = { startInReference, stopInReference, startInMatchingStream, stopInMatchinStream, score };
		matches.add(match);
	}

	/**
	 * returns an array with the following information {startInReference, float
	 * stopInReference, float startInMatchingStream,float
	 * stopInMatchinStream,float score} The score is the number of aligned
	 * matching fingerprints.
	 * 
	 * @param i
	 *            the match to return
	 * @return Return the match.
	 */
	public float[] getMatch(int i) {
		return matches.get(i).clone();
	}

	public int getNumberOfMatches() {
		return matches.size();
	}

}