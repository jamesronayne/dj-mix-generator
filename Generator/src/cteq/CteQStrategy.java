/***************************************************************************
*                                                     								*
* DJ Mix Generator                                       					 		*
* Copyright (C) 2016 - James Ronayne                      							*
*                                                                          			*
* This program is free software: you can redistribute it and/or modify     			*
* it under the terms of the GNU Affero General Public License as           			*
* published by the Free Software Foundation, either version 3 of the       			*
* License, or (at your option) any later version.                          			*
*                                                                          			*
* This program is distributed in the hope that it will be useful,          			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of           			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            			*
* GNU Affero General Public License for more details.                      			*
*                                                                          			*
* You should have received a copy of the GNU Affero General Public License 			*
* along with this program.  If not, see <http://www.gnu.org/licenses/>     			*
*                                                                          			*
*************************************************************************************
*																					*
*																					*
*    8888888b. 888888      888b     d888 d8b                                      	*
*    888  "Y88b  "88b      8888b   d8888 Y8P                                      	*
*    888    888   888      88888b.d88888                                          	*
*    888    888   888      888Y88888P888 888 888  888                             	*
*    888    888   888      888 Y888P 888 888 `Y8bd8P'                             	*
*    888    888   888      888  Y8P  888 888   X88K                               	*
*    888  .d88P   88P      888   "   888 888 .d8""8b.                             	*
*    8888888P"    888      888       888 888 888  888                             	*
*               .d88P                                                             	*
*             .d88P"                                                              	*
*            888P"                                                                	*
*     .d8888b.                                             888                    	*
*    d88P  Y88b                                            888                    	*
*    888    888                                            888                    	*
*    888         .d88b.  88888b.   .d88b.  888d888 8888b.  888888 .d88b.  888d888 	*
*    888  88888 d8P  Y8b 888 "88b d8P  Y8b 888P"      "88b 888   d88""88b 888P"   	*
*    888    888 88888888 888  888 88888888 888    .d888888 888   888  888 888     	*
*    Y88b  d88P Y8b.     888  888 Y8b.     888    888  888 Y88b. Y88..88P 888     	*
*     "Y8888P88  "Y8888  888  888  "Y8888  888    "Y888888  "Y888 "Y88P"  888     	*
*                                                                                 	*
************************************************************************************/

package cteq;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.writer.WaveHeader;
import util.AudioStitcher;
import util.Decoder;
import util.NextTrack;
import javafx.collections.ObservableList;

/**
 * @author James Ronayne
 * 
 */
public class CteQStrategy {

	// Used when printing playlist
	ObservableList<NextTrack> selectedTracklist;
	ObservableList<String> precedingTracklist;
	ObservableList<String> trailingTracklist;

	String outputFile;
	String outputPlaylist;
	String outputFolder = "E:\\backup\\";
	String outputTemp;

	ArrayList<String> mixPaths;

	String[] stitchedTimestamps;
	double[] stitchedTimestampsSeconds;

	ObservableList<NextTrack> selected;

	double progressInc;

	boolean printPlaylist = false;
	boolean adjustGain = false;

	/**
	 * sets data to perform generation with no playlist must be done before
	 * calling generate()
	 * 
	 * @param outputFile
	 *            output filename to be used for mix
	 * @param outputPlaylist
	 *            output filename to be used for playlist
	 * @param outputFolder
	 *            output folder where mix and playlist will be saved
	 * @param outputTemp
	 *            temp filename to use
	 * @param mixPaths
	 *            ArrayList of paths to the mixes to be stitched
	 */
	public void setData(String outputFile, String outputFolder, String outputTemp, ArrayList<String> mixPaths,
			boolean adjustGain) {

		this.adjustGain = adjustGain;
		printPlaylist = false;

		this.outputFile = outputFile;
		this.outputFolder = outputFolder;
		this.outputTemp = outputTemp;
		this.mixPaths = mixPaths;

		stitchedTimestamps = new String[mixPaths.size()];
		stitchedTimestampsSeconds = new double[mixPaths.size() - 1];
	}

	/**
	 * sets data to perform generation must be done before calling generate()
	 * 
	 * @param selectedTracklist
	 *            used to print playlist
	 * @param precedingTracklist
	 *            used to print playlist
	 * @param trailingTracklist
	 *            used to print playlist
	 * @param nextTracklist
	 *            used to print playlist
	 * @param outputFile
	 *            output filename to be used for mix
	 * @param outputPlaylist
	 *            output filename to be used for playlist
	 * @param outputFolder
	 *            output folder where mix and playlist will be saved
	 * @param outputTemp
	 *            temp filename to use
	 * @param mixPaths
	 *            ArrayList of paths to the mixes to be stitched
	 */
	public void setDataAndPrintPlaylist(ObservableList<NextTrack> selectedTracklist,
			ObservableList<String> precedingTracklist, ObservableList<String> trailingTracklist, String outputFile,
			String outputPlaylist, String outputFolder, String outputTemp, ArrayList<String> mixPaths,
			boolean adjustGain) {

		this.adjustGain = adjustGain;
		printPlaylist = true;

		this.selectedTracklist = selectedTracklist;
		this.precedingTracklist = precedingTracklist;
		this.trailingTracklist = trailingTracklist;
		this.outputFile = outputFile;
		this.outputPlaylist = outputPlaylist;
		this.outputFolder = outputFolder;
		this.outputTemp = outputTemp;
		this.mixPaths = mixPaths;

		stitchedTimestamps = new String[mixPaths.size()];
		stitchedTimestampsSeconds = new double[mixPaths.size() - 1];
	}

	/**
	 * returns double array of timestamp locations
	 * 
	 * @return
	 */
	public double[] getStitchedTimestampsSeconds() {
		return stitchedTimestampsSeconds;
	}

	/**
	 * Returns the location finsihed mix will be saved to
	 * 
	 * @return
	 */
	public String getOutputFile() {
		return outputFile;
	}

	/**
	 * Returns the location playlist will be saved to
	 * 
	 * @return
	 */
	public String getOutputPlaylist() {
		return outputPlaylist;
	}

	/**
	 * After data has been set with setData() will generate the entire mix
	 * 
	 * @return True for success and False for failure
	 */
	public boolean generate() {

		List<double[]> offsets = new ArrayList<double[]>();
		final int eventPointsPerSecondForQuery = 8;
		int branchingFactor = 4;

		for (int i = 1; i < mixPaths.size(); i++) {

			// If either file does not exist match cannot be done
			if (!new File(mixPaths.get(i)).exists() || !new File(mixPaths.get(i - 1)).exists())
				return false;

			// 2 is the minimum allowed number of aligned matches
			double[] offset = synchronize(3, mixPaths.get(i - 1), mixPaths.get(i));
			if (offset != null) {
				offsets.add(offset);
			} else {
				// failed to synchronise
				System.err.println("Failed to synch: \n" + mixPaths.get(i - 1) + "\n" + mixPaths.get(i));
				return false;
			}
		}

		CteQFingerprintProcessor constantQ = new CteQFingerprintProcessor(eventPointsPerSecondForQuery,
				branchingFactor);
		int size = constantQ.getFFTlength();
		int overlap = 0;

		// resets temp file in case it has already been used
		File temp = new File(outputTemp);
		temp.delete();
		try {
			temp.createNewFile();
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		double totalSecs = 0.0;

		for (int i = 0; i < mixPaths.size(); i++) {

			float offsetFrom = 0;
			float offsetTo = 0;
			if (i == 0) {
				// If the first file
				offsetFrom = 0;
				offsetTo = new Float(offsets.get(i)[0]);
			} else if (i != (mixPaths.size() - 1)) {
				// If not the last file
				offsetFrom = new Float(offsets.get(i - 1)[1]);
				offsetTo = new Float(offsets.get(i)[0]) - offsetFrom;
			} else {
				// If the last file
				offsetFrom = new Float(offsets.get(i - 1)[1]);
			}

			// converts stitching timestamps to hour, minutes and seconds for
			// printing out playlist
			totalSecs += offsetTo;

			stitchedTimestamps[i] = secsToTime(totalSecs);

			if (i != mixPaths.size() - 1)
				stitchedTimestampsSeconds[i] = totalSecs;

			// Hasn't found correct alignment as duration is negative
			if (offsetTo < 0) {
				System.err.println("Found incorrect allignment as duration was negative: \nHalt generating...");
				return false;
			}

			AudioDispatcher adp = null;

			float gain = 0f;
			if (adjustGain) {
				gain = Decoder.getGain(mixPaths.get(i));
				System.out.println("gain adjustment: " + gain);
			} else {
				gain = 0f;
			}

			// use offsetTo unless last track
			if (i != (mixPaths.size() - 1)) {
				adp = Decoder.getDecodedStreamFromTo(mixPaths.get(i), 44100, size, overlap, offsetFrom, offsetTo, gain);
			} else {
				adp = Decoder.getDecodedStreamFromTo(mixPaths.get(i), 44100, size, overlap, offsetFrom, 0.0001f, gain);
			}

			AudioStitcher as = new AudioStitcher(temp);
			adp.addAudioProcessor(as);
			adp.run();

			System.out.println("finished decoding: " + (i + 1));
		}

		try {
			finish(temp);
		} catch (IOException e) {
			// failed to synchronise
			return false;
		}

		// completed generation successfully
		return true;
	}

	/**
	 * Finishes mix generation by printing playlist and formating mix into a WAV
	 * file and closing any files still open
	 * 
	 * @param temp
	 *            the file which holds the finished mix in raw PCM format
	 * @throws IOException
	 */
	private void finish(File temp) throws IOException {
		File outputReset = new File(outputFile);
		outputReset.delete();

		outputReset.createNewFile();

		RandomAccessFile output = new RandomAccessFile(outputFile, "rw");

		formatPCMWithWavHeader(output, temp);

		if (printPlaylist)
			outputPlaylist();

		System.out.println("done");

		output.close();

		temp.delete();
	}

	int minimumMatchesThreshold = 100;

	private int minimumAlignedMatchesThreshold = 2; // default 2

	/**
	 * Synchronises reference and other
	 * 
	 * @param minimumAlignedMatchesThreshold
	 *            the minimum allowed aligned matches to be a match typically 3
	 *            is a good number
	 * @param reference
	 *            absolute path to first audio file
	 * @param other
	 *            absolute path to second audio file
	 * @return double[] that contains offset for referencePrints in double[0]
	 *         and otherPrints in double[1] in seconds
	 */
	@SuppressWarnings("unchecked")
	public double[] synchronize(int minimumAlignedMatchesThreshold, String reference, String other) {
		this.minimumAlignedMatchesThreshold = minimumAlignedMatchesThreshold;

		String directoryRef = (reference.substring(0, reference.length() - 5));
		File fingerprintsRef = new File(directoryRef + "fingerprintsFinal.data");

		List<CteQFingerprint> referencePrints = null;
		// If fingerprints already been generated then load them instead of
		// extracting
		if (fingerprintsRef.exists()) {
			// Loads in precomputed offsets
			try {
				FileInputStream fileIn = new FileInputStream(fingerprintsRef);
				ObjectInputStream in = new ObjectInputStream(fileIn);
				referencePrints = (ArrayList<CteQFingerprint>) in.readObject();
				System.out.println("size: " + referencePrints.size());
				in.close();
				fileIn.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			// extract fingerprints for reference audio
			referencePrints = extractFingerprints(reference);
		}

		// extract fingerprints for other audio streams
		List<CteQFingerprint> otherPrints = null;

		String directoryOther = (other.substring(0, other.length() - 5));
		File fingerprintsOther = new File(directoryOther + "fingerprintsFinal.data");

		// If fingerprints already been generated then load them instead of
		// extracting
		if (fingerprintsOther.exists()) {
			// Loads in precomputed offsets
			try {
				FileInputStream fileIn = new FileInputStream(fingerprintsOther);
				ObjectInputStream in = new ObjectInputStream(fileIn);
				otherPrints = (ArrayList<CteQFingerprint>) in.readObject();
				in.close();
				fileIn.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			otherPrints = extractFingerprints(other);
		}

		// match the reference with all other streams
		double[] offsets = match(referencePrints, otherPrints);
		return offsets;
	}

	/**
	 * Given audio file and returns List of fingerprints
	 * 
	 * @param Absolute
	 *            path to audio file to fingerprint
	 * @return List of CteQFingerprint
	 */
	private List<CteQFingerprint> extractFingerprints(String resource) {
		CteQFingerprintProcessor constantQ;

		int eventPointsPerSecondForStorage = 8;
		int branchingFactor = 1;

		constantQ = new CteQFingerprintProcessor(eventPointsPerSecondForStorage, branchingFactor);
		int size = constantQ.getFFTlength();
		int overlap = size - constantQ.getHopSize();

		AudioDispatcher adp = Decoder.getDecodedStream(resource, 44100, size, overlap);
		adp.addAudioProcessor(constantQ);
		adp.run();

		return new ArrayList<CteQFingerprint>(constantQ.getFingerprints());
	}

	private double prevMinOtherFingerprintTime = 0;

	/**
	 * finds offsets that align referencePrints with otherPrints
	 * 
	 * @param referencePrints
	 * @param otherPrints
	 * @return double[] that contains offset for referencePrints in double[0]
	 *         and otherPrints in double[1] in seconds
	 */
	private double[] match(List<CteQFingerprint> referencePrints, List<CteQFingerprint> otherPrints) {
		// create a map with the fingerprint hash as key, and fingerprint object
		// as value.
		// Warning: only a single object is kept for each hash.
		HashMap<Integer, CteQFingerprint> referenceHash = fingerprintsToHash(referencePrints);
		double[] offsets = null;

		HashMap<Integer, CteQFingerprint> otherHash = fingerprintsToHash(otherPrints);
		offsets = findBestOffset(referenceHash, otherHash);

		if (offsets != null) {
			prevMinOtherFingerprintTime = offsets[1];
			return offsets;
		} else {
			return null;
		}

	}

	/**
	 * Returns HashMap of each fingerprint to its associated hash
	 * 
	 * @param fingerprints
	 *            List of fingerprints to generate HashMap for
	 * @return
	 */
	public HashMap<Integer, CteQFingerprint> fingerprintsToHash(List<CteQFingerprint> fingerprints) {
		HashMap<Integer, CteQFingerprint> hash = new HashMap<>();
		for (CteQFingerprint fingerprint : fingerprints) {
			hash.put(fingerprint.hash(), fingerprint);
		}
		return hash;
	}

	/**
	 * F
	 * 
	 * @param referenceHash
	 * @param otherHash
	 * @return
	 */
	private double[] findBestOffset(HashMap<Integer, CteQFingerprint> referenceHash,
			HashMap<Integer, CteQFingerprint> otherHash) {
		// key is the offset, value a list of fingerprint objects.
		HashMap<Integer, List<CteQFingerprint>> mostPopularOffsets = new HashMap<Integer, List<CteQFingerprint>>();

		int maxAlignedOffsets = 0;

		// Used to help group matches so that they can be searched later for
		// best match
		CteQSyncMatch match = new CteQSyncMatch();

		// iterate each fingerprint in the reference stream
		for (Map.Entry<Integer, CteQFingerprint> entry : referenceHash.entrySet()) {
			// if the fingerprint is also present in the other stream
			if (otherHash.containsKey(entry.getKey())) {

				CteQFingerprint refFingerprint = entry.getValue();
				CteQFingerprint otherFingerprint = otherHash.get(entry.getKey());
				int offset = refFingerprint.t1 - otherFingerprint.t1;
				// add the offset to the hashmap, if it is not already in the
				// hashmap.
				if (!mostPopularOffsets.containsKey(offset)) {
					mostPopularOffsets.put(offset, new ArrayList<CteQFingerprint>());
				}
				// add the reference and other fingerprint to the list.
				// add the other fingerprint to the list.
				// the reference fingerprints are at even, the other are odd
				// indexes.
				mostPopularOffsets.get(offset).add(refFingerprint);
				mostPopularOffsets.get(offset).add(otherFingerprint);

				// keep a max count
				if (mostPopularOffsets.get(offset).size() / 2 > maxAlignedOffsets) {
					maxAlignedOffsets = mostPopularOffsets.get(offset).size() / 2;
				}
			}
		}

		if (maxAlignedOffsets >= minimumAlignedMatchesThreshold) {
			// remove each offset below the minimum threshold
			List<Integer> offsetsToRemove = new ArrayList<Integer>();
			for (Map.Entry<Integer, List<CteQFingerprint>> entry : mostPopularOffsets.entrySet()) {
				if (entry.getValue().size() / 2 < minimumAlignedMatchesThreshold) {
					offsetsToRemove.add(entry.getKey());
				}
			}
			for (Integer offsetToRemove : offsetsToRemove) {
				mostPopularOffsets.remove(offsetToRemove);
			}
			// now only 'real' matching offsets remain in the list. These need
			// to be refined and reported.
			for (Map.Entry<Integer, List<CteQFingerprint>> entry : mostPopularOffsets.entrySet()) {
				// int offset = entry.getKey();//offset in blocks
				List<CteQFingerprint> matchingPairs = entry.getValue();

				int minRefTimeIndex = Integer.MAX_VALUE;
				int minOtherTimeIndex = Integer.MAX_VALUE;
				int maxRefTimeIndex = Integer.MIN_VALUE;
				int maxOtherTimeIndex = Integer.MIN_VALUE;
				// find where the offset matches start and stop
				for (int i = 0; i < matchingPairs.size(); i += 2) {
					CteQFingerprint refFingerprint = matchingPairs.get(i);
					CteQFingerprint otherFingerprint = matchingPairs.get(i + 1);
					minRefTimeIndex = Math.min(refFingerprint.t1, minRefTimeIndex);
					minOtherTimeIndex = Math.min(otherFingerprint.t1, minOtherTimeIndex);
					maxRefTimeIndex = Math.max(refFingerprint.t1, maxRefTimeIndex);
					maxOtherTimeIndex = Math.max(otherFingerprint.t1, maxOtherTimeIndex);
				}

				double refinedOffset = -100000;

				int samplerate = 44100;
				float fftHopSizesS = 1536 / (float) samplerate;

				refinedOffset = (((maxRefTimeIndex - maxOtherTimeIndex) * fftHopSizesS)
						+ ((minRefTimeIndex - minOtherTimeIndex) * fftHopSizesS)) / 2.0;

				if (refinedOffset == -100000) {
					System.out.println("No matching offset found using crosscovariance.");
					refinedOffset = ((minRefTimeIndex - minOtherTimeIndex) * fftHopSizesS);
				}

				double minReferenceFingerprintTime = minRefTimeIndex * fftHopSizesS;
				double minOtherFingerprintTime = minOtherTimeIndex * fftHopSizesS;
				double maxReferenceFingerprintTime = maxRefTimeIndex * fftHopSizesS;
				double maxOtherFingerprintTime = maxOtherTimeIndex * fftHopSizesS;

				match.addMatch((float) minReferenceFingerprintTime, (float) maxReferenceFingerprintTime,
						(float) minOtherFingerprintTime, (float) maxOtherFingerprintTime, matchingPairs.size() / 2);

				String message = String.format(
						"[%.1fs - %.1fs] matches [%.1fs - %.1fs] with an offset of %.4fs (%d matches)",
						minReferenceFingerprintTime, maxReferenceFingerprintTime, minOtherFingerprintTime,
						maxOtherFingerprintTime, refinedOffset, matchingPairs.size() / 2);

				System.out.println(message);
			}

			// finding greatest match and returning
			float highestScore = 0;
			int greatestMatch = 0;
			for (int i = 0; i < match.getNumberOfMatches(); i++) {
				float[] aMatch = match.getMatch(i);

				// aMatch[4] > highestScore
				// && (aMatch[0] > prevMinOtherFingerprintTime)
				if ((aMatch[4] > highestScore) && (aMatch[0] > prevMinOtherFingerprintTime)) {
					highestScore = aMatch[4];
					greatestMatch = i;
				}
			}

			float refTime = match.getMatch(greatestMatch)[0];
			float otherTime = match.getMatch(greatestMatch)[2];

			System.out.println(refTime + " " + prevMinOtherFingerprintTime);
			System.out.println("highest score: " + highestScore);

			double[] offsets = { refTime, otherTime };

			return offsets;

		} else {
			System.out.println("No offsets found");
		}
		return null;
	}

	/**
	 * Converts seconds to a hours, minutes and seconds string
	 * 
	 * @param totalSecs
	 *            number of seconds to convert
	 * @return String of seconds converted to hours, minutes and seconds
	 */
	private String secsToTime(double totalSecs) {
		int hours = (int) (totalSecs / 3600);
		int minutes = (int) ((totalSecs % 3600) / 60);
		int seconds = (int) (totalSecs % 60);
		return String.format("%02d:%02d:%02d", hours, minutes, seconds);
	}

	/**
	 * formats raw PCM file into output file with correct WAV Header
	 * 
	 * @param output
	 *            filename to output finished WAV to
	 * @param filesToStitch
	 *            The Raw PCM data of stitched mixes
	 */
	private void formatPCMWithWavHeader(RandomAccessFile output, File filesToStitch) {
		int HEADER_LENGTH = 44;

		WaveHeader wh = new WaveHeader(WaveHeader.FORMAT_PCM, (short) 1, 44100, (short) 16,
				(int) filesToStitch.length());

		try {
			output.write(new byte[HEADER_LENGTH]);
		} catch (IOException e) {
			e.printStackTrace();
		}

		FileInputStream fin = null;
		try {

			fin = new FileInputStream(filesToStitch);

			byte fileContent[] = new byte[1024];

			while (fin.read(fileContent) != -1) {
				output.write(fileContent);
			}

			ByteArrayOutputStream header = new ByteArrayOutputStream();
			try {
				wh.write(header);
				output.seek(0);
				output.write(header.toByteArray());

				output.close();
				fin.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading file: " + ioe);
		}

	}

	/**
	 * Outputs playlist for mix to outputPlaylist including time stamps for
	 * where mixes were stitched
	 */
	public void outputPlaylist() {
		int trackNo = 1;

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(outputPlaylist)));

			for (int i = 0; i < precedingTracklist.size(); i++) {
				writer.write(trackNo + ". " + precedingTracklist.get(i) + "\r\n");
				trackNo++;
			}

			int newPlaylistCount = 0;
			String prevNextTrack = "";
			for (int i = 0; i < selectedTracklist.size(); i++) {
				if (!prevNextTrack.equals(selectedTracklist.get(i).getPlaylistId()) && i > 1) {
					writer.write(trackNo + ". " + selectedTracklist.get(i).getTrackname() + " [stitched at: "
							+ stitchedTimestamps[newPlaylistCount] + "]\r\n");
					newPlaylistCount++;
				} else {
					writer.write(trackNo + ". " + selectedTracklist.get(i).getTrackname() + "\r\n");
				}
				trackNo++;

				prevNextTrack = selectedTracklist.get(i).getPlaylistId();
			}

			for (int i = 0; i < trailingTracklist.size(); i++) {
				writer.write(trackNo + ". " + trailingTracklist.get(i) + "\r\n");
				trackNo++;
			}

			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
