package testing;

import org.junit.Assert;
import org.junit.Test;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.DetermineDurationProcessor;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import util.Decoder;

/**
 * @author James Ronayne
 */
public class DecoderTest {
	String audioTestFile = "testAudio/test.wav";

	@Test
	public void getGainTest() {
		float gain = Decoder.getGain(audioTestFile);

		Assert.assertTrue(gain == -0.0);
	}

	@Test
	public void getDecodedStreamTest() {
		AudioDispatcher ad = Decoder.getDecodedStream(audioTestFile, 44100, 1024, 0);

		Assert.assertTrue(ad.getFormat().getEncoding() == TarsosDSPAudioFormat.Encoding.PCM_SIGNED);

		DetermineDurationProcessor durationProcessor = new DetermineDurationProcessor();
		ad.addAudioProcessor(durationProcessor);
		ad.run();

		Assert.assertTrue(Math.abs(durationProcessor.getDurationInSeconds() - 397.78) < 0.1);
	}

	@Test
	public void getDecodedStreamFromToTest() {
		// decode 10 seconds of audio
		AudioDispatcher ad = Decoder.getDecodedStreamFromTo(audioTestFile, 44100, 1024, 0, 0f, 10f, 0.0f);

		DetermineDurationProcessor durationProcessor = new DetermineDurationProcessor();
		ad.addAudioProcessor(durationProcessor);
		ad.run();

		// check decoded audio duration is 10 seconds
		Assert.assertTrue(Math.abs(durationProcessor.getDurationInSeconds() - 10) < 0.1);
	}
}
