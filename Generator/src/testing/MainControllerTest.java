package testing;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import controller.MainController;

/**
 * @author James Ronayne
 */
public class MainControllerTest {

	@Test
	public void noConsecutiveDupsTest() {
		// Only function that can be properly tested due to JavaFX application
		// thread
		// Does a simple check to make sure consecutive duplicates are removed
		// from array list
		ArrayList<String> test = new ArrayList<String>();

		test.add("a");
		test.add("test");
		test.add("test");
		test.add("a");

		ArrayList<String> result = MainController.noConsecutiveDups(test);

		// Resulting array should only be of size 3 after removing one "test"
		// string
		Assert.assertTrue(result.size() == 3);
	}
}
