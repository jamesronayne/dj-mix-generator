package testing;

import java.io.File;
import java.util.Arrays;

import org.junit.Test;

import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;

import org.junit.Assert;
import util.AudioStitcher;

/**
 * @author James Ronayne
 */
public class AudioStitchTest {

	@Test
	public void stitchTest() {
		AudioStitcher as = new AudioStitcher(new File("test"));
		TarsosDSPAudioFormat format = new TarsosDSPAudioFormat(44100, 32, 1, true, false);
		AudioEvent ae = new AudioEvent(format);

		// fills float array with dummy data
		float[] floatArr = new float[1024];
		Arrays.fill(floatArr, 12f);

		ae.setFloatBuffer(floatArr);
		as.process(ae);
		as.processingFinished();

		File out = new File("test");
		long length = out.length();
		out.delete();

		// Check length of audio created is same length as float array length *
		// 4 as floats are 32bit numbers
		Assert.assertEquals(floatArr.length * 4, length);
	}
}
