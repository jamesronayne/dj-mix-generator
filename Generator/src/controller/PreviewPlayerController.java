package controller;

import java.io.File;

import cteq.CteQStrategy;
import javafx.animation.AnimationTimer;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * 
 * @author James Ronayne
 *
 */
public class PreviewPlayerController {

	/**
	 * 
	 */
	private class Offset {
		double offset;

		Offset(double offset) {
			this.offset = offset;
		}

		public double getOffset() {
			return offset;
		}
	}

	@FXML
	Button previousButtton, playPauseButton, nextButton;
	@FXML
	Slider seekbar;
	@FXML
	Label currentTime, duration;
	@FXML
	ListView<Offset> offsets;

	MediaPlayer player;
	String filename;
	double[] timestamps;
	CteQStrategy strat;
	int timestampIndex = 0;
	boolean toggle = false;

	// Constructor
	public PreviewPlayerController(CteQStrategy strat) {
		this.strat = strat;
	}

	/**
	 * 
	 */
	@FXML
	private void initialize() {

		Image playPauseButtonImage = new Image(getClass().getClassLoader().getResourceAsStream("view/Play.png"));
		playPauseButton.setGraphic(new ImageView(playPauseButtonImage));

		Image previousButttonImage = new Image(getClass().getClassLoader().getResourceAsStream("view/Previous.png"));
		previousButtton.setGraphic(new ImageView(previousButttonImage));

		Image nextButtonImage = new Image(getClass().getClassLoader().getResourceAsStream("view/Next.png"));
		nextButton.setGraphic(new ImageView(nextButtonImage));

		timestamps = strat.getStitchedTimestampsSeconds();

		Callback<ListView<Offset>, ListCell<Offset>> offsetCallback = new Callback<ListView<Offset>, ListCell<Offset>>() {
			@Override
			public ListCell<Offset> call(ListView<Offset> param) {
				ListCell<Offset> cell = new ListCell<Offset>() {

					@Override
					protected void updateItem(Offset item, boolean empty) {
						super.updateItem(item, empty);
						if (item != null) {
							setText("Stitch at: " + formatTimeSeconds(item.getOffset()));
						} else {
							setText("");
						}
					}
				};
				return cell;
			}
		};
		offsets.setCellFactory(offsetCallback);

		// When offset is selected player skips to 5 seconds before offset and
		// clear selection
		offsets.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<?> observableValue, Object oldValue, Object newValue) {

				Offset offset = (Offset) newValue;
				player.seek(new Duration((offset.getOffset() - 5.0) * 1000));
			}
		});

		for (int i = 0; i < timestamps.length; i++) {
			offsets.getItems().add(new Offset(timestamps[i]));
		}

		Media media = new Media(new File(strat.getOutputFile()).toURI().toString());
		player = new MediaPlayer(media);

		// Ran after player is ready so that duration can be obtained
		player.setOnReady(new Runnable() {
			@Override
			public void run() {
				seekbar.setMax(media.getDuration().toSeconds());
				seekbar.setMin(0.0);

				duration.setText(formatTimeSeconds(media.getDuration().toSeconds()));

				new Thread(new Runnable() {
					@Override
					public void run() {
						while (true) {
							try {
								Thread.sleep(500);

								double totalSeconds = player.getCurrentTime().toSeconds();
								seekbar.setValue(totalSeconds);
							} catch (InterruptedException ex) {
								Thread.currentThread().interrupt();
							}
						}
					}
				}).start();

				seekbar.setOnMouseClicked(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {
						Bounds b1 = seekbar.getLayoutBounds();
						double mouseX = event.getSceneX();
						double percent = (((b1.getMinX() + mouseX) * 100) / b1.getMaxX());
						percent -= 2;

						player.seek(new Duration(((percent) / 100) * media.getDuration().toMillis()));
					}
				});

				final LongProperty lastUpdate = new SimpleLongProperty();

				final long minUpdateInterval = 1000000000; // nanoseconds. Set
															// to higher number
															// to slow output.

				AnimationTimer timer = new AnimationTimer() {
					@Override
					public void handle(long now) {
						try {
							if (now - lastUpdate.get() > minUpdateInterval) {
								double totalSeconds = player.getCurrentTime().toSeconds();
								currentTime.setText(formatTimeSeconds(totalSeconds));
							}
						} catch (NullPointerException e) {
							e.printStackTrace();
							System.out.println("here");
						}
					}
				};

				timer.start();
			}
		});
	}

	private String formatTimeSeconds(double totalSeconds) {

		int hours = (int) (totalSeconds / 3600);
		int minutes = (int) ((totalSeconds % 3600) / 60);
		int seconds = (int) (totalSeconds % 60);
		String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
		return timeString;
	}

	@FXML
	void previous() {
		if (timestampIndex <= 0) {
			player.seek(new Duration(0));
			timestampIndex = 0;
		} else {
			timestampIndex--;
			player.seek(new Duration((timestamps[timestampIndex] - 5.0) * 1000));
		}
	}

	/**
	 * 
	 */
	@FXML
	void playPause() {
		if (toggle) {
			// pause

			Image playPauseButtonImage = new Image(getClass().getClassLoader().getResourceAsStream("view/Play.png"));
			playPauseButton.setGraphic(new ImageView(playPauseButtonImage));

			player.pause();
			toggle = false;
		} else {
			// play

			Image playPauseButtonImage = new Image(getClass().getClassLoader().getResourceAsStream("view/Pause.png"));
			playPauseButton.setGraphic(new ImageView(playPauseButtonImage));

			player.play();
			toggle = true;
		}
	}

	@FXML
	void next() {
		if (timestampIndex > timestamps.length - 1) {

		} else {
			timestampIndex++;
			player.seek(new Duration((timestamps[timestampIndex] - 5.0) * 1000));
		}
	}

}
