/***************************************************************************
*                                                     								*
* DJ Mix Generator                                       					 		*
* Copyright (C) 2016 - James Ronayne                      							*
*                                                                          			*
* This program is free software: you can redistribute it and/or modify     			*
* it under the terms of the GNU Affero General Public License as           			*
* published by the Free Software Foundation, either version 3 of the       			*
* License, or (at your option) any later version.                          			*
*                                                                          			*
* This program is distributed in the hope that it will be useful,          			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of           			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            			*
* GNU Affero General Public License for more details.                      			*
*                                                                          			*
* You should have received a copy of the GNU Affero General Public License 			*
* along with this program.  If not, see <http://www.gnu.org/licenses/>     			*
*                                                                          			*
*************************************************************************************
*																					*
*																					*
*    8888888b. 888888      888b     d888 d8b                                      	*
*    888  "Y88b  "88b      8888b   d8888 Y8P                                      	*
*    888    888   888      88888b.d88888                                          	*
*    888    888   888      888Y88888P888 888 888  888                             	*
*    888    888   888      888 Y888P 888 888 `Y8bd8P'                             	*
*    888    888   888      888  Y8P  888 888   X88K                               	*
*    888  .d88P   88P      888   "   888 888 .d8""8b.                             	*
*    8888888P"    888      888       888 888 888  888                             	*
*               .d88P                                                             	*
*             .d88P"                                                              	*
*            888P"                                                                	*
*     .d8888b.                                             888                    	*
*    d88P  Y88b                                            888                    	*
*    888    888                                            888                    	*
*    888         .d88b.  88888b.   .d88b.  888d888 8888b.  888888 .d88b.  888d888 	*
*    888  88888 d8P  Y8b 888 "88b d8P  Y8b 888P"      "88b 888   d88""88b 888P"   	*
*    888    888 88888888 888  888 88888888 888    .d888888 888   888  888 888     	*
*    Y88b  d88P Y8b.     888  888 Y8b.     888    888  888 Y88b. Y88..88P 888     	*
*     "Y8888P88  "Y8888  888  888  "Y8888  888    "Y888888  "Y888 "Y88P"  888     	*
*                                                                                 	*
************************************************************************************/

package controller;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

import cteq.CteQStrategy;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import util.NextTrack;

/**
 * 
 * @author James Ronayne
 *
 */
public class MainController {
	// Establish the connection to the database
	private static String url = "jdbc:mysql://127.0.0.1:3306/mix";
	private static Connection conn;

	@FXML
	AnchorPane stage;
	@FXML
	AnchorPane anchor;
	@FXML
	ScrollPane sp;

	@FXML
	TextField artistSearch;
	@FXML
	TextField trackSearch;

	@FXML
	ListView<NextTrack> selectedTracklist;
	@FXML
	ListView<String> precedingTracklist;
	@FXML
	ListView<String> trailingTracklist;
	@FXML
	ListView<NextTrack> nextTracklist;

	@FXML
	ProgressBar progressBar;

	@FXML
	HBox statusHbox;

	@FXML
	Button searchButton;
	@FXML
	Button generateButton;

	String currentArtist, currentTrack, currentID, firstTrackPlaylistID, lastTrackPlaylistID;
	boolean isFirstSelection = true;

	int ran;

	// Default constructor
	public MainController() {
	}

	/**
	 * Connects to MySQL database and sets table callbacks
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@FXML
	private void initialize() {
		try {
			// Step 1: "Load" the JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection(url, "root", "djgenerator");
		} catch (SQLException e) {
			System.out.println("Failed to connect to mySQL server");
			e.getMessage();
		} catch (ClassNotFoundException e) {
			System.out.println("mySQL driver class not found exiting...");
			e.printStackTrace();
		}

		// Allows you to search by clicking enter in trackSearch TextField
		trackSearch.setOnKeyReleased(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					searchSeedSong();
				}
			}
		});

		// callback used to generate what is displayed in the list views
		Callback<ListView<NextTrack>, ListCell<NextTrack>> nextTrackCallback = new Callback<ListView<NextTrack>, ListCell<NextTrack>>() {
			@Override
			public ListCell<NextTrack> call(ListView<NextTrack> param) {
				ListCell<NextTrack> cell = new ListCell<NextTrack>() {

					@Override
					protected void updateItem(NextTrack item, boolean empty) {
						super.updateItem(item, empty);
						if (item != null) {
							setText(item.getTrackname() + " (" + item.getMixName() + ")(" + item.getPlaylistId() + ")");
						} else {
							setText("");
						}
					}
				};
				return cell;
			}
		};

		// sets callbacks
		nextTracklist.setCellFactory(nextTrackCallback);
		selectedTracklist.setCellFactory(nextTrackCallback);

		// Adds change listener for when user selects next track
		nextTracklist.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
				try {
					NextTrack track = nextTracklist.getSelectionModel().getSelectedItem();
					if (isFirstSelection) {
						firstTrackPlaylistID = track.getPlaylistId();
						updatePreceding();
						isFirstSelection = false;
					}

					lastTrackPlaylistID = track.getPlaylistId();

					String[] trackname = track.getTrackname().split("-");
					currentArtist = trackname[0];
					currentTrack = trackname[1];

					currentID = track.getTrackId();

					// Makes sure observableLists aren't modified while in
					// notification
					Platform.runLater(() -> {
						selectedTracklist.getItems().add(track);

						// Checks if a offset can be found for track selected if
						// not removes option from list
						if (checkCompatibility()) {

							updateTrailing();
							updateNextSongs();
						} else {
							nextTracklist.getSelectionModel().clearSelection();
							selectedTracklist.getItems().remove(track);
							nextTracklist.getItems().remove(track);
						}
					});
				} catch (NullPointerException e) {

				}
			}
		});

		// Adds change listener for when user selects one of the selected tracks
		// and what to move back
		selectedTracklist.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {

				NextTrack track = (NextTrack) newValue;

				lastTrackPlaylistID = track.getPlaylistId();

				String[] trackname = track.getTrackname().split("-");
				currentArtist = trackname[0];
				currentTrack = trackname[1];

				currentID = track.getTrackId();

				selectedTracklist.getItems().add(track);

				int index = selectedTracklist.getSelectionModel().selectedIndexProperty().get();

				// Makes sure observableLists aren't modified while in
				// notification
				Platform.runLater(() -> {

					selectedTracklist.getItems().remove(index + 1, selectedTracklist.getItems().size());

					if (index == 0) {
						isFirstSelection = true;
					}

					updateTrailing();
					updateNextSongs();

					selectedTracklist.getSelectionModel().clearSelection();
				});
			}
		});

	}

	/**
	 * remove instances of more than one of the same mix so that analysis is
	 * done correctly
	 * 
	 * @param List
	 *            of mixes to be stitched
	 * @return Same list with no consecutive duplicates
	 */
	public static ArrayList<String> noConsecutiveDups(ArrayList<String> input) {

		ArrayList<String> newList = new ArrayList<String>();

		// Always add first value
		newList.add(input.get(0));

		// Iterate the remaining values
		for (int i = 1; i < input.size(); i++) {

			// Compare current value to previous
			if (!input.get(i - 1).equals(input.get(i))) {
				newList.add(input.get(i));
			}
		}

		return newList;
	}

	/**
	 * Loads preview player after mix preview has been generated
	 */
	@FXML
	private void loadPreview() {
		setup();

		// Runs generation on another thread so that main stage does not become
		// unresponsive
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (strat.generate()) {
					if (!Thread.currentThread().isInterrupted()) {
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								loadView();
							}
						});
					}
				}
			}
		}).start();
	}

	/**
	 * Loads view of the preview player
	 */
	private void loadView() {

		try {

			FXMLLoader loader = new FXMLLoader(
					this.getClass().getClassLoader().getResource("view/PreviewPlayerView.fxml"));

			PreviewPlayerController controller = new PreviewPlayerController(strat);

			loader.setController(controller);

			AnchorPane anchorPane = loader.load();

			Scene scene = new Scene(anchorPane);
			Stage previewStage = new Stage();
			previewStage.setScene(scene);

			previewStage.setTitle("DJ Lazier - Preview Player");// Adds icon for
																// application
			previewStage.getIcons().add(new Image("view/DJ.png"));
			previewStage.setResizable(false);
			// Cleans up files when preview player is closed
			previewStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				public void handle(WindowEvent we) {
					controller.player.stop();
					controller.player.dispose();

					new File(controller.strat.getOutputFile()).delete();
					new File(controller.strat.getOutputPlaylist()).delete();
					previewStage.close();
					System.out.println("preview player is closing");
				}
			});

			previewStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	String outputFile;
	String outputPlaylist;
	String outputFolder = "E:\\backup\\";
	String outputTemp;

	String[] stitchedTimestamps;
	double[] stitchedTimestampsSeconds;

	ObservableList<NextTrack> selected;

	CteQStrategy strat = new CteQStrategy();

	double progressInc;

	/**
	 * Generates mix based on currently selected tracks
	 */
	@FXML
	private void generate() {

		progressBar.setVisible(true);
		progressBar.setProgress(0.0f);

		if (!setup()) {
			progressBar.setVisible(false);
			return;
		}

		progressInc = 1.0d / 2.0d;

		progressBar.setProgress(progressBar.getProgress() + progressInc);

		if (strat.generate()) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Success");
			alert.setHeaderText("Mix was generated successfully");
			alert.setContentText("Output file is: " + outputFile + "\nPlaylist file is: " + outputPlaylist);

			alert.showAndWait();

			progressBar.setProgress(progressBar.getProgress() + progressInc);
			progressBar.setVisible(false);
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Couldn't stitch files");
			alert.setContentText(":(");

			alert.showAndWait();
			progressBar.setVisible(false);
		}
		System.out.println("outfile: " + outputFile);
		System.out.println("playlist file: " + outputPlaylist);
	}

	/**
	 * sets up data for mix generation including file names and mixes selected
	 * 
	 * @return true if everything went smoothly false if a error occurred
	 */
	private boolean setup() {
		strat = new CteQStrategy();

		updateRan();
		ArrayList<String> arr = getMixList();

		strat.setDataAndPrintPlaylist(selectedTracklist.getItems(), precedingTracklist.getItems(),
				trailingTracklist.getItems(), outputFile, outputPlaylist, outputFolder, outputTemp, arr, false);

		return true;
	}

	/**
	 * Grabs the mix list with no duplicates from the currently selected tracks
	 * 
	 * @return The mix path list with no duplicates
	 */
	private ArrayList<String> getMixList() {
		selected = selectedTracklist.getItems();
		String[] playlistIds = new String[selected.size() - 1];

		for (int i = 1; i < selected.size(); i++) {
			playlistIds[i - 1] = selected.get(i).getPlaylistId();
		}

		ArrayList<String> arr = new ArrayList<String>();

		for (int i = 0; i < playlistIds.length; i++) {

			try {
				Statement select = conn.createStatement();
				String sql = "SELECT * FROM mixes WHERE playlist_id = " + playlistIds[i];

				ResultSet rs = select.executeQuery(sql);

				rs.next();

				String filepath = rs.getString("Filepath");

				System.out.println(rs.getString("URL"));

				// removing .mp3 extension as was not stored with file extension
				// to allow more file formats
				arr.add(filepath.substring(0, filepath.length() - 4));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		arr = noConsecutiveDups(arr);

		for (int i = 0; i < arr.size(); i++) {
			System.out.println(arr.get(i));
		}

		stitchedTimestamps = new String[arr.size()];
		stitchedTimestampsSeconds = new double[arr.size()];

		ArrayList<String> arr2 = arr;
		return arr2;
	}

	/**
	 * Checks if offsets can be found for mixes currently selected
	 * 
	 * @return true if offsets can be found false if not
	 */
	private boolean checkCompatibility() {

		ArrayList<String> mixPaths = getMixList();

		if (mixPaths.size() == 1)
			return true;

		for (int i = 1; i < mixPaths.size(); i++) {

			int firstID = Integer.parseInt(mixPaths.get(i - 1).substring(16, mixPaths.get(i - 1).lastIndexOf('/')));
			int secondID = Integer.parseInt(mixPaths.get(i).substring(16, mixPaths.get(i).lastIndexOf('/')));
			System.out.println("firstID: " + firstID + " secondID: " + secondID);

			// If either file does not exist match cannot be done
			if (!new File(mixPaths.get(i)).exists() || !new File(mixPaths.get(i - 1)).exists())
				return false;

			// 3 is the minimum allowed number of aligned matches
			double[] offset = strat.synchronize(3, mixPaths.get(i - 1), mixPaths.get(i));
			if (offset != null) {
			} else {
				// failed to synchronise
				System.err.println("Failed to synch: \n" + mixPaths.get(i - 1) + "\n" + mixPaths.get(i));
				return false;
			}
		}

		return true;
	}

	/**
	 * Allows user to select output folder for audio and tracklist folder
	 */
	@FXML
	private void chooseOutputFolder() {
		DirectoryChooser fileChooser = new DirectoryChooser();
		fileChooser.setTitle("Choose a folder");
		outputFolder = fileChooser.showDialog(new Stage()).getAbsolutePath();

		updateRan();
	}

	/**
	 * Updates output files with random names
	 */
	private void updateRan() {
		ran = Math.abs(new Random().nextInt());
		outputFile = outputFolder + "\\" + String.valueOf(ran) + ".wav";
		outputPlaylist = outputFolder + "\\" + String.valueOf(ran) + ".txt";
		outputTemp = outputFolder + "\\" + String.valueOf(ran) + ".temp";

		System.out.println(outputFile + " " + outputPlaylist + " " + outputTemp);
	}

	/**
	 * Searches mySQL database for seed song adds to selected track list and
	 * updates next tracks
	 */
	@FXML
	private void searchSeedSong() {
		String artistSearchStr = artistSearch.getText();
		String trackSearchStr = trackSearch.getText();
		isFirstSelection = true;

		// query sql database if not there insert
		try {

			Statement select = conn.createStatement();
			String sql = "SELECT * FROM tracks WHERE ( artist LIKE \"" + artistSearchStr + "\" AND name LIKE \""
					+ trackSearchStr + "\" )";

			ResultSet rs = select.executeQuery(sql);

			// If resultSet is not empty then retrieve
			if (rs.next()) {
				String artist = rs.getString("artist");
				String track = rs.getString("name");
				int id = rs.getInt("track_id");

				ObservableList<NextTrack> names = FXCollections
						.observableArrayList(new NextTrack(artist + " - " + track, "", id, ""));
				selectedTracklist.setItems(names);

				currentArtist = artist;
				currentTrack = track;
				currentID = rs.getString("track_id");

				select.close();

				updateNextSongs();
			} else {
				System.out.println("Error: Couldn't find track please try another");
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText("Couldn't find track please try another");
				alert.setContentText("Be specific with the name and use captial letters");

				alert.showAndWait();
			}

			select.close();

		} catch (SQLException e) {
			System.err.println("D'oh! Got an exception!");
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Updates preceding track list with songs that preceded the seed song
	 */
	private void updatePreceding() {
		// reset precedingTracklist
		ObservableList<String> reset = FXCollections.observableArrayList();
		precedingTracklist.setItems(reset);
		// query sql database
		try {

			Statement select = conn.createStatement();
			String sql = "SELECT * FROM playlists_songs WHERE track_id = " + currentID + " AND playlist_id = "
					+ firstTrackPlaylistID;

			ResultSet rs = select.executeQuery(sql);

			while (rs.next()) {
				String playlistID = rs.getString("playlist_id");
				int trackNo = rs.getInt("track_number");
				Statement select2 = null;
				for (int i = trackNo - 1; i >= 0; i--) {
					select2 = conn.createStatement();
					sql = "SELECT * FROM playlists_songs WHERE playlist_id = " + playlistID + " AND track_number = "
							+ i;
					ResultSet nextTrackRS = select2.executeQuery(sql);

					if (nextTrackRS.next()) {
						int trackID = nextTrackRS.getInt("track_id");
						String sqlTrack = "SELECT * FROM tracks WHERE track_id = " + trackID;
						Statement select3 = conn.createStatement();
						ResultSet nextTrackRS2 = select3.executeQuery(sqlTrack);
						nextTrackRS2.next();
						String nextArtist = nextTrackRS2.getString("artist");
						String nextTrack = nextTrackRS2.getString("name");

						precedingTracklist.getItems().add(0, nextArtist + " - " + nextTrack);

						select3.close();
					} else {
						System.err.println("no next track");
					}

				}

				select2.close();
			}

			select.close();

		} catch (SQLException e) {
			System.err.println("D'oh! Got an exception updating preceding!");
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Updates trailing track list with the songs which follow the last selected
	 * song
	 */
	private void updateTrailing() {
		// reset trailingTracklist
		ObservableList<String> names = FXCollections.observableArrayList();
		trailingTracklist.setItems(names);

		// query sql database
		try {

			Statement select = conn.createStatement();
			String sql = "SELECT * FROM playlists_songs WHERE track_id = " + currentID + " AND playlist_id = "
					+ lastTrackPlaylistID;

			ResultSet rs = select.executeQuery(sql);

			while (rs.next()) {
				String playlistID = rs.getString("playlist_id");
				int trackNo = rs.getInt("track_number");

				Statement select2 = conn.createStatement();
				sql = "SELECT * FROM playlists_songs WHERE playlist_id = " + playlistID + " AND track_number > "
						+ trackNo;
				ResultSet nextTrackRS = select2.executeQuery(sql);

				while (nextTrackRS.next()) {
					int trackID = nextTrackRS.getInt("track_id");
					String sqlTrack = "SELECT * FROM tracks WHERE track_id = " + trackID;
					Statement select3 = conn.createStatement();
					ResultSet nextTrackRS2 = select3.executeQuery(sqlTrack);
					nextTrackRS2.next();
					String nextArtist = nextTrackRS2.getString("artist");
					String nextTrack = nextTrackRS2.getString("name");

					trailingTracklist.getItems().add(nextArtist + " - " + nextTrack);

					select3.close();
				}
				select2.close();
			}

			select.close();

		} catch (SQLException e) {
			System.err.println("D'oh! Got an exception updating preceding!");
			System.err.println(e.getMessage());
		}
	}

	/**
	 * next songs list with songs that are available to come after the last
	 * selected song
	 */
	private void updateNextSongs() {
		// reset nextTracklist
		ObservableList<NextTrack> names = FXCollections.observableArrayList();
		nextTracklist.setItems(names);

		// query sql database
		try {

			Statement select = conn.createStatement();
			String sql = "SELECT * FROM playlists_songs WHERE track_id = " + currentID;

			ResultSet rs = select.executeQuery(sql);

			while (rs.next()) {
				String playlistID = rs.getString("playlist_id");

				int trackNo = rs.getInt("track_number");

				Statement select2 = conn.createStatement();
				sql = "SELECT * FROM playlists_songs WHERE playlist_id = " + playlistID + " AND track_number = "
						+ (trackNo + 1);
				ResultSet nextTrackRS = select2.executeQuery(sql);

				if (nextTrackRS.next()) {
					int trackID = nextTrackRS.getInt("track_id");
					String sqlTrack = "SELECT * FROM tracks WHERE track_id = " + trackID;
					Statement select3 = conn.createStatement();
					nextTrackRS = select3.executeQuery(sqlTrack);
					nextTrackRS.next();
					String nextArtist = nextTrackRS.getString("artist");
					String nextTrack = nextTrackRS.getString("name");

					Statement select4 = conn.createStatement();
					String sqlMix = "SELECT * FROM mixes WHERE playlist_id = " + playlistID;
					ResultSet mixRS = select4.executeQuery(sqlMix);
					mixRS.next();
					String mixName = mixRS.getString("Name");

					if (!checkForMoreThanOneShared(playlistID)) {
						nextTracklist.getItems()
								.add(new NextTrack(nextArtist + " - " + nextTrack, playlistID, trackID, mixName));
					} else {
						continue;
					}

					select3.close();
				} else {
					System.err.println("No next track for playlistID: " + playlistID);
				}
				select2.close();
			}

			select.close();
		} catch (SQLException e) {
			System.err.println("D'oh! Got an exception updating next songs!");
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Checks for more than one shared track between mixes if so then discard as
	 * analysis cannot be trusted to find correct track
	 * 
	 * @param playlistID
	 *            of mix to check with lastTrackPlaylistID
	 * @return false for not more than one shared track and true if more than
	 *         one
	 */
	private boolean checkForMoreThanOneShared(String playlistID) {
		// if the same playlist return false as all tracks will be shared anyway
		if (playlistID.equals(lastTrackPlaylistID)) {
			return false;
		}

		Statement select;
		try {
			select = conn.createStatement();
			String sql = "SELECT * FROM playlists_songs WHERE playlist_id = " + playlistID;

			ResultSet rs = select.executeQuery(sql);

			ArrayList<String> playlistSongs = new ArrayList<String>();
			ArrayList<String> comparisonPlaylist = new ArrayList<String>();

			while (rs.next()) {
				playlistSongs.add(rs.getString("track_id"));
			}

			select = conn.createStatement();
			sql = "SELECT * FROM playlists_songs WHERE playlist_id = " + lastTrackPlaylistID;

			rs = select.executeQuery(sql);

			while (rs.next()) {
				comparisonPlaylist.add(rs.getString("track_id"));
			}

			int noSharedSongs = 0;

			for (String trackID : playlistSongs) {
				if (comparisonPlaylist.contains(trackID)) {
					noSharedSongs++;
				}
			}

			// If more than 1 shared track then return false and discard this
			// next track
			if (noSharedSongs > 1 && selectedTracklist.getItems().size() > 2) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Else 1 shared track so return false and keep next track
		return false;
	}
}
