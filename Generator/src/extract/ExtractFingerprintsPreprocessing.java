/***************************************************************************
*                                                     								*
* DJ Mix Generator                                       					 		*
* Copyright (C) 2016 - James Ronayne                      							*
*                                                                          			*
* This program is free software: you can redistribute it and/or modify     			*
* it under the terms of the GNU Affero General Public License as           			*
* published by the Free Software Foundation, either version 3 of the       			*
* License, or (at your option) any later version.                          			*
*                                                                          			*
* This program is distributed in the hope that it will be useful,          			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of           			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            			*
* GNU Affero General Public License for more details.                      			*
*                                                                          			*
* You should have received a copy of the GNU Affero General Public License 			*
* along with this program.  If not, see <http://www.gnu.org/licenses/>     			*
*                                                                          			*
*************************************************************************************
*																					*
*																					*
*    8888888b. 888888      888b     d888 d8b                                      	*
*    888  "Y88b  "88b      8888b   d8888 Y8P                                      	*
*    888    888   888      88888b.d88888                                          	*
*    888    888   888      888Y88888P888 888 888  888                             	*
*    888    888   888      888 Y888P 888 888 `Y8bd8P'                             	*
*    888    888   888      888  Y8P  888 888   X88K                               	*
*    888  .d88P   88P      888   "   888 888 .d8""8b.                             	*
*    8888888P"    888      888       888 888 888  888                             	*
*               .d88P                                                             	*
*             .d88P"                                                              	*
*            888P"                                                                	*
*     .d8888b.                                             888                    	*
*    d88P  Y88b                                            888                    	*
*    888    888                                            888                    	*
*    888         .d88b.  88888b.   .d88b.  888d888 8888b.  888888 .d88b.  888d888 	*
*    888  88888 d8P  Y8b 888 "88b d8P  Y8b 888P"      "88b 888   d88""88b 888P"   	*
*    888    888 88888888 888  888 88888888 888    .d888888 888   888  888 888     	*
*    Y88b  d88P Y8b.     888  888 Y8b.     888    888  888 Y88b. Y88..88P 888     	*
*     "Y8888P88  "Y8888  888  888  "Y8888  888    "Y888888  "Y888 "Y88P"  888     	*
*                                                                                 	*
************************************************************************************/

package extract;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import be.tarsos.dsp.AudioDispatcher;
import cteq.CteQFingerprint;
import cteq.CteQFingerprintProcessor;
import util.Decoder;

/**
 * 
 * @author James Ronayne
 *
 */
public class ExtractFingerprintsPreprocessing {
	// Establish the connection to the database
	static String url = "jdbc:mysql://127.0.0.1:3306/mix";
	static Connection conn;

	private static class FingerprinterThread extends Thread {
		private String file;

		public FingerprinterThread(String file) {
			this.file = file;
		}

		public void run() {
			System.out.println("starting thread...");
			// removing .mp3 extension as was not stored with file extension to
			// allow more file formats
			String audioPath = (file.substring(0, file.length() - 4));

			String directory = (file.substring(0, file.length() - 9));

			ArrayList<CteQFingerprint> fingerprints = extractFingerprints(audioPath);

			FileOutputStream fileOut;
			try {
				File a = new File(directory + "fingerprints.ser");
				a.createNewFile();
				fileOut = new FileOutputStream(directory + "fingerprints.ser");
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
				out.writeObject(fingerprints);
				out.close();
				fileOut.close();
				System.out.println("Serialized data is saved in " + directory + "fingerprints.ser");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}

	public static void main(String[] args) {
		try {
			// Step 1: "Load" the JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection(url, "root", "djgenerator");

		} catch (SQLException e) {
			System.out.println("Failed to connect to mysql server");
			e.getMessage();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ExecutorService executor = Executors.newFixedThreadPool(5);

		try {
			Statement select = conn.createStatement();
			String sql = "SELECT * FROM mixes";

			ResultSet rs = select.executeQuery(sql);

			while (rs.next()) {

				String filepath = rs.getString("Filepath");

				Runnable worker = new FingerprinterThread(filepath);
				executor.execute(worker);

			}
			executor.shutdown();
			executor.awaitTermination(100L, TimeUnit.SECONDS);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	private static ArrayList<CteQFingerprint> extractFingerprints(String resource) {
		CteQFingerprintProcessor constantQ;

		int eventPointsPerSecondForStorage = 8;
		int branchingFactor = 1;

		constantQ = new CteQFingerprintProcessor(eventPointsPerSecondForStorage, branchingFactor);
		int size = constantQ.getFFTlength();
		int overlap = size - constantQ.getHopSize();

		AudioDispatcher adp = Decoder.getDecodedStream(resource, 44100, size, overlap);
		adp.addAudioProcessor(constantQ);
		adp.run();

		return new ArrayList<CteQFingerprint>(constantQ.getFingerprints());
	}
}
